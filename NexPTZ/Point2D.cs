﻿using System;

namespace NexPTZ
{
    public struct Point2D
    {
        public double X;
        public double Y;
    }
}