﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    /// <summary>
    /// Interface describing an interface for interacting with a camera's Presets.
    /// </summary>
    public interface IPresets
    {
        /// <summary>
        /// All available presets, by name.
        /// </summary>
        List<string> AllPresets { get; }

        bool CanGotoPreset { get; set; }
        bool CanSetPreset { get; set; }
        bool CanRemovePreset { get; set; }
        bool CanNamePreset { get; set; }

        int MaxPresets { get; set; }

        bool CanAddPreset(string ID);

        /// <summary>
        /// Adds a new preset.
        /// </summary>
        /// <remarks>Some cameras do not support naming the preset.</remarks>
        /// <param name="ID"></param>
        void AddPreset(string ID);

        /// <summary>
        /// Resets the position of a given preset.
        /// </summary>
        /// <param name="ID"></param>
        void SetPreset(string ID);

        /// <summary>
        /// Remove a given preset.
        /// </summary>
        /// <param name="ID"></param>
        void RemovePreset(string ID);

        /// <summary>
        /// Pan/tilt/zoom to a given preset
        /// </summary>
        /// <param name="ID"></param>
        void GoToPreset(string ID);
    }
}
