﻿using System;

namespace NexPTZ.Protocols.Amcrest
{
    internal class ProtocolAttribute : Attribute
    {
        public string Author { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}