﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    public enum PTZCommand
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        UPLEFT,
        UPRIGHT,
        DOWNRIGHT,
        DOWNLEFT,
        ZOOMIN,
        ZOOMOUT,
        RESET,
        ABSMOVE,
        RELMOVE,
        GOTOPRESET, 
        SETPRESET,  //Overwrites a preset
        CLEARPRESET,//Deletes/clears preset
        ADDPRESET   //Creates a new preset
    }
}
