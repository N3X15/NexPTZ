﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    /// <summary>
    /// Provides focus adjustment functionality for the Camera.
    /// </summary>
    public interface IFocus
    {
        /// <summary>
        /// Enables or disables autofocus.
        /// </summary>
        bool Autofocus { get; set; }

        /// <summary>
        /// Gets or sets current focus level.
        /// </summary>
        double Current { get; set; }

        double Min { get; }
        double Max { get; }
    }
}
