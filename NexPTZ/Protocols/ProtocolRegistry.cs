﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    public class ProtocolRegistry
    {
        public static Dictionary<string, Type> AllProtocols = new Dictionary<string, Type>();

        public static void Preload()
        {
            AllProtocols.Clear();
            foreach (Type t in typeof(NexPTZ.Protocols.Protocol).Assembly.GetTypes())
            {
                if (t.IsSubclassOf(typeof(Protocols.Protocol)))
                {
                    AllProtocols[t.Name] = t;
                }
            }
        }

        public static Type GetProtocolType(string name)
        {
            return AllProtocols[name];
        }

        public static Protocol GetProtocol(string name)
        {
            return (Protocol)(GetProtocolType(name).GetConstructor(Type.EmptyTypes).Invoke(new object[0]));
        }
    }
}
