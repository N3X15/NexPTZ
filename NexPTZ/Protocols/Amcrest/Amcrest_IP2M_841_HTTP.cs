﻿/**
 * Amcrest IP2M-841 Communication Protocol for NexPTZ.
 *
 * Made for my Amcrest IP2M-841B.
 * 
 * Reference Material: https://support.amcrest.com/hc/en-us/articles/213155128-Amcrest-HTTP-API-SDK-IP2M-841-
 * Because Amcrest have been lax in keeping that updated, I also had to do some reverse-engineering with Wireshark and IE inspector.
 *
 * MIT License
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

namespace NexPTZ.Protocols.Amcrest
{
    [ProtocolAttribute(Name = "Amcrest IP2M-841 HTTP API", Author = "N3X15", Description = "Based off of Amcrest HTTP API docs, rev 2.12.")]
    public class Amcrest_IP2M_841_HTTP : Protocol
    {
        public enum APIMethod
        {
            Up,
            Down,
            Left,
            Right,

            ZoomWide,
            ZoomTele,

            FocusNear,
            FocusFar,

            IrisLarge,
            IrisSmall,

            GotoPreset,
            SetPreset,
            ClearPreset,

            StartTour,
            StopTour,

            LeftUp,
            RightUp,
            LeftDown,
            RightDown,

            AddTour,
            DelTour,
            ClearTour,

            AutoPanOn,
            AutoPanOff,

            SetLeftLimit,
            SetRightLimit,

            AutoScanOn,
            AutoScanOff,

            SetPatternBegin,
            SetPatternEnd,
            StartPattern,
            StopPattern,
            ClearPattern,

            /// <summary>
            /// Go to position
            /// X, Y, Zoom
            /// </summary>
            Position,

            AuxOn,
            AuxOff,

            Menu,
            Exit,
            Enter,
            MenuUp,
            MenuDown,
            MenuLeft,
            MenuRight,

            Reset,

            /// <summary>
            /// Address of light controller, Light number, switch
            /// </summary>
            LightController,

            /// <summary>
            /// H ang [0,360], V ang [0,90], Zoom, Speed[1,8]
            /// </summary>
            PositionABS,

            Continuously,

        }

        private Dictionary<PTZCommand, APIMethod> Translator = new Dictionary<PTZCommand, APIMethod> {
            {PTZCommand.RELMOVE, APIMethod.Position},
            {PTZCommand.DOWN, APIMethod.Down },
            {PTZCommand.DOWNLEFT, APIMethod.LeftDown },
            {PTZCommand.DOWNRIGHT, APIMethod.RightDown },
            {PTZCommand.LEFT, APIMethod.Left },
            {PTZCommand.RESET, APIMethod.Reset },
            {PTZCommand.RIGHT, APIMethod.Right },
            {PTZCommand.UP, APIMethod.Up },
            {PTZCommand.UPLEFT, APIMethod.LeftUp },
            {PTZCommand.UPRIGHT, APIMethod.RightUp },
            {PTZCommand.ZOOMIN, APIMethod.ZoomTele },
            {PTZCommand.ZOOMOUT, APIMethod.ZoomWide }
        };

        public enum APIAction
        {
            start,
            stop
        }

        private string BuildURI(string subpath, Dictionary<string, string> actions = null)
        {
            var uri = string.Format("http://{0}:{1}", Camera.Address, Camera.Port) + subpath;
            int written = 0;
            if (actions != null && actions.Count > 0)
            {
                foreach (var kvp in actions)
                {
                    if (written == 0)
                        uri += "?";
                    else
                        uri += "&";
                    written++;
                    uri += kvp.Key + "=" + Uri.EscapeUriString(kvp.Value);
                }
            }
            return uri;
        }

        private string BuildPTZURI(APIMethod method, APIAction action, params object[] args)
        {
            var dict = new Dictionary<string, string>();
            dict["action"] = Enum.GetName(typeof(APIAction), action);
            dict["channel"] = "1";
            dict["code"] = Enum.GetName(typeof(APIMethod), method);
            for (int i = 0; i < 4; i++)
            {
                string o = "0";
                if (i < args.Length)
                    o = args[i].ToString();
                dict["arg" + (i + 1).ToString()] = o;
            }
            return BuildURI("/cgi-bin/ptz.cgi", dict);
        }

        private WebRequest BuildRequest(string url)
        {
            var req = WebRequest.Create(url);
            var uri = new Uri(url);
            var creds = new CredentialCache();
            creds.Add(
              new Uri(uri.GetLeftPart(UriPartial.Authority)), // request url's host
              "Digest",  // authentication type 
              new NetworkCredential(Camera.Username, Camera.Password) // credentials 
            );
            req.Credentials = creds;
            return req;
        }

        /// <summary>
        /// Gets JSON data from the camera and turns it into an object.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private dynamic GetFromJSON(WebRequest req)
        {
            Console.WriteLine("Fetching {0}...", req.RequestUri);
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            {
                StreamReader rdr = new StreamReader(response.GetResponseStream());
                string data = rdr.ReadToEnd();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                return jss.Deserialize<dynamic>(data);

            }
        }

        /// <summary>
        /// Gets AKV data from the camera and converts it into an object.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private dynamic GetFromAKV(WebRequest req, string filename="")
        {
            Console.WriteLine("Fetching {0}...", req.RequestUri);
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            {
                StreamReader rdr = new StreamReader(response.GetResponseStream());
                string data = rdr.ReadToEnd();
                if(filename!="")
                    File.WriteAllText(filename+".akv", data);

                dynamic parsed = ParseAKV(data);
                //JavaScriptSerializer jss = new JavaScriptSerializer();
                //Dump(jss.Serialize(parsed), "json");

                return parsed;
            }
        }
        #region Amcrest Keyvalue (AKV) Parser
        // My apologies to the space archaeologists from the future who have to figure out what the fuck this is doing.
        // I made it while high on migraine meds and prescription-strength Aleve.
        // supposed to take something like:
        //   data.key1=value
        //   data.key2[0]=a
        //   data.key2[1]=b
        // and turn it into
        //   {data:{key1:"value", key2:["a", "b"]}}
        // However, it looks more like:
        //   {data:{key1:"value", key2:{"0":"a", "1":"b"}}}
        // I hope in time that I can fix it.

        private dynamic ParseAKV(string data)
        {
            dynamic parent = new Dictionary<dynamic, dynamic>();
            foreach (string line in data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (line.Trim() == "Error")
                {
                    throw new Exception(data);
                }
                string[] chunks = line.Split(new char[] { '=' }, 2);
                SetAKV(parent, chunks[0].Split(new char[] { '.', '[' }, StringSplitOptions.None), ParseAKVValue(chunks[1]));
            }
            return parent;
        }

        private void SetAKV(dynamic parent, string[] key, dynamic value)
        {
            Console.WriteLine(string.Join(",", key));
            dynamic currentKey = key[0];
            if (currentKey.EndsWith("]"))
            {
                currentKey = int.Parse(currentKey.Substring(0, currentKey.Length - 1));
            }
            if (key.Length == 1)
            {
                parent[currentKey] = value;
                return;
            }
            if (!parent.ContainsKey(currentKey))
            {
                parent[currentKey] = new Dictionary<dynamic, dynamic>();
            }
            SetAKV(parent[currentKey], key.Skip(1).ToArray(), value);
        }

        private dynamic ParseAKVValue(string v)
        {
            if (v == "true")
                return true;
            if (v == "false")
                return false;
            int i = 0;
            if (int.TryParse(v, out i))
                return i;
            double d = 0;
            if (double.TryParse(v, out d))
                return d;
            return v;
        }
        #endregion

        private void GetNothing(WebRequest req)
        {
            Console.WriteLine("Executing {0}...", req.RequestUri);
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            {
                StreamReader rdr = new StreamReader(response.GetResponseStream());
                rdr.ReadToEnd();
            }
        }

        public override void Connect()
        {
            // Get the capabilities of this camera.
            var req = BuildRequest(BuildURI("/cgi-bin/ptz.cgi", new Dictionary<string, string> {
                { "action", "getCurrentProtocolCaps" },
                { "channel", "1" } }));

            var data = GetFromAKV(req, "ptz.getCurrentProtocolCaps");
            //this.CanFlip = data["caps"]["Flip"];
            //this.CanIris = data["caps"]["Iris"];
            //this.CanPan = data["caps"]["Pan"];
            Presets = new Amcrest_IP2M_841_HTTP.PresetsAPI(this);
            ((PresetsAPI)Presets).LoadPTZCaps(data);

            //if (data["caps"]["Zoom"])
            //{
            Zoom = new Amcrest_IP2M_841_HTTP.ZoomAPI(this);
            //}
            //if (data["caps"]["Focus"])
            //{
            this.Focus = new Amcrest_IP2M_841_HTTP.FocusAPI(this);
            ((FocusAPI)Focus).LoadPTZCaps(data);
            //this.refreshFocusStatus();
            //}

            Move = new Amcrest_IP2M_841_HTTP.MoveAPI(this);
            ((MoveAPI)Move).LoadPTZCaps(data);
            req = BuildRequest(BuildURI("/cgi-bin/devVideoInput.cgi", new Dictionary<string, string> {
                { "action", "getCaps" },
                { "channel", "1" } }));
            var vi_caps = GetFromAKV(req, "devVideoInput.getCaps");
        }

        private void refreshFocusStatus()
        {
            // Broken as of 9/23/2017 due to unexplained error 500 on IP2M-841B 2.520.AC00.18.R, Build Date: 2017-06-29.
            // Duplicated on SUPER SEKRIT /RPC protocol, as well, so it's FUCKED.
            return;
            if (Focus != null && Zoom != null)
            {
                dynamic focus_data = GetFromAKV(BuildRequest(BuildURI("/cgi-bin/devVideoInput.cgi", new Dictionary<string, string> {
                    {"action","getFocusStatus" },
                    {"channel", "1" }
                })),"devVideoInput");
                if (Focus != null)
                {
                    ((FocusAPI)Focus)._level = (double)focus_data["status"]["Focus"];
                    ((FocusAPI)Focus)._autofocus = focus_data["status"]["Status"] == "Autofocus";
                }
                if (Zoom != null)
                {
                    ((ZoomAPI)Zoom).level = focus_data["status"]["Zoom"];
                }
            }
        }

        public override void LoadPresets()
        {
            // Loads presets from the camera.
            var req = BuildRequest(BuildURI("/cgi-bin/ptz.cgi", new Dictionary<string, string> {
                { "action", "getPresets" },
                { "channel", "1" } }));
            ((PresetsAPI)Presets).LoadFrom(GetFromAKV(req));
        }

        public override void SendCommand(PTZCommand cmd, bool stop, params object[] args)
        {
            switch (cmd)
            {
                case PTZCommand.RELMOVE:
                    {
                        Point2D p = (Point2D)args[0];
                        SendPTZCommand(APIMethod.Position, stop, p.X, p.Y, args[1]);
                    }
                    break;
                case PTZCommand.ABSMOVE:
                    {
                        Point2D p = (Point2D)args[0];
                        SendPTZCommand(APIMethod.PositionABS, stop, p.X, p.Y, args[1]);
                    }
                    break;

                case PTZCommand.RESET:
                    break;

                default:
                    SendPTZCommand(Translator[cmd], stop, 0, args[0], 0);
                    break;
            }
        }

        private void SendPTZCommand(APIMethod method, bool stop, params object[] args)
        {
            GetNothing(BuildRequest(BuildPTZURI(method, stop ? APIAction.stop : APIAction.start, args)));
        }

        private class FocusAPI : IFocus
        {
            private Amcrest_IP2M_841_HTTP protocol;
            internal double _level;
            internal bool _autofocus;

            public FocusAPI(Amcrest_IP2M_841_HTTP protocol)
            {
                this.protocol = protocol;
                this._level = 0d;
                this._autofocus = true;

            }

            internal void SendFocusLevel(double level)
            {
                protocol.GetNothing(protocol.BuildRequest(protocol.BuildURI("/cgi-bin/devVideoInput.cgi", new Dictionary<string, string> {
                    { "action", "adjustFocus" },
                    { "focus", level.ToString() },
                    {"zoom", "-1" } })));
                _level = level;
            }

            internal void SendAutofocus()
            {
                _level = -1;
                protocol.GetNothing(protocol.BuildRequest(protocol.BuildURI("/cgi-bin/devVideoInput.cgi", new Dictionary<string, string> {
                    { "action", "autoFocus" } })));
                protocol.refreshFocusStatus();
            }

            internal void LoadPTZCaps(dynamic data)
            {
                // ???
            }

            bool IFocus.Autofocus
            {
                get
                {
                    return _autofocus;
                }

                set
                {
                    if (value)
                        SendAutofocus();
                    else
                        SendFocusLevel(_level);
                }
            }

            double IFocus.Current
            {
                get
                {
                    return _level;
                }

                set
                {
                    SendFocusLevel(value);
                    _level = value;
                }
            }

            double IFocus.Max
            {
                get
                {
                    return 1d;
                }
            }

            double IFocus.Min
            {
                get
                {
                    return 0d;
                }
            }
        }

        private class PresetsAPI : IPresets
        {
            private Amcrest_IP2M_841_HTTP protocol;
            private List<string> _all_presets;
            private Dictionary<string, int> _map;
            private int MaxPresets;
            private int _max_presets;

            public PresetsAPI(Amcrest_IP2M_841_HTTP protocol)
            {
                this.protocol = protocol;
                Reset();
            }
            private void Reset()
            {
                _map = new Dictionary<string, int>();
                _all_presets = new List<string>();
            }
            internal void LoadFrom(dynamic data)
            {
                Reset();
                foreach (var kvp in data["presets"])
                {
                    var preset = kvp.Value;
                    _map[preset["Name"]] = preset["Index"];
                    _all_presets.Add(preset["Name"]);
                }
            }

            List<string> IPresets.AllPresets
            {
                get
                {
                    return _all_presets;
                }
            }

            bool IPresets.CanGotoPreset
            {
                get
                {
                    return true;
                }

                set
                { }
            }

            bool IPresets.CanSetPreset
            {
                get
                {
                    return true;
                }

                set
                {

                }
            }

            bool IPresets.CanRemovePreset
            {
                get
                {
                    return true;
                }

                set
                {

                }
            }

            bool IPresets.CanNamePreset
            {
                get
                {
                    return false;
                }

                set
                {

                }
            }

            int IPresets.MaxPresets
            {
                get
                {
                    return _max_presets;
                }

                set
                {
                    _max_presets = value;
                }
            }

            void IPresets.AddPreset(string ID)
            {
                int firstUnused = 0;
                for (int i = 1; i <= MaxPresets; i++)
                {
                    if (_map.ContainsValue(i))
                        continue;
                    firstUnused = i;
                    break;
                }
                if (firstUnused > 0)
                {
                    protocol.SendPTZCommand(APIMethod.SetPreset, false, 0, firstUnused, 0);
                    protocol.LoadPresets();
                }
            }

            void IPresets.RemovePreset(string ID)
            {
                protocol.SendPTZCommand(APIMethod.ClearPreset, false, 0, _map[ID], 0);
                protocol.LoadPresets();
            }

            void IPresets.SetPreset(string ID)
            {
                protocol.LoadPresets();
            }

            void IPresets.GoToPreset(string ID)
            {
                protocol.SendPTZCommand(APIMethod.GotoPreset, false, 0, _map[ID], 0);
            }

            bool IPresets.CanAddPreset(string ID)
            {
                return _all_presets.Count < MaxPresets;
            }

            internal void LoadPTZCaps(dynamic data)
            {
                //this.MinPresets = data["caps"]["PresetMin"];
                this.MaxPresets = data["caps"]["PresetMax"];
            }
        }

        private class ZoomAPI : IZoom
        {
            private Amcrest_IP2M_841_HTTP protocol;
            internal double level;

            public ZoomAPI(Amcrest_IP2M_841_HTTP protocol)
            {
                this.protocol = protocol;
                level = 0d;
            }

            bool IZoom.CanDirectZoom { get { return false; } }
            bool IZoom.CanRelativeZoom { get { return true; } }

            internal void SendZoomLevel(double lvl)
            {
                protocol.GetNothing(protocol.BuildRequest(protocol.BuildURI("/cgi-bin/devVideoInput.cgi", new Dictionary<string, string> {
                    { "action", "adjustFocus" },
                    { "focus", "-1" },
                    {"zoom", lvl.ToString("G2") } })));
                this.level = lvl;
            }

            double IZoom.Current
            {
                get
                {
                    return level;
                }

                set
                {
                    SendZoomLevel(value);
                }
            }

            double IZoom.Max
            {
                get
                {
                    return 1d;
                }
            }

            double IZoom.Min
            {
                get
                {
                    return 0d;
                }
            }

            void IZoom.ZoomIn(bool stop)
            {
                protocol.SendPTZCommand(APIMethod.ZoomTele, stop);
                if (stop)
                    protocol.refreshFocusStatus();
            }

            void IZoom.ZoomOut(bool stop)
            {
                protocol.SendPTZCommand(APIMethod.ZoomWide, stop);
                if (stop)
                    protocol.refreshFocusStatus();
            }
        }

        private class MoveAPI : IMove
        {
            private Amcrest_IP2M_841_HTTP protocol;
            private int _speed_max;
            private int _speed_min;

            public MoveAPI(Amcrest_IP2M_841_HTTP protocol)
            {
                this.protocol = protocol;
            }

            bool IMove.CanAbsoluteMove
            {
                get
                {
                    return true;
                }
            }

            bool IMove.CanDiagonalMove
            {
                get
                {
                    return true;
                }
            }

            bool IMove.CanRelativeMove
            {
                get
                {
                    return false; // Broken, undocumented.
                }
            }

            int IMove.SpeedMax
            {
                get
                {
                    return _speed_max;
                }
            }

            int IMove.SpeedMin
            {
                get
                {
                    return _speed_min;
                }
            }

            internal void LoadPTZCaps(dynamic data)
            {
                this._speed_min = data["caps"]["PanSpeedMin"];
                this._speed_max = data["caps"]["PanSpeedMax"];
            }
        }
    }
}
