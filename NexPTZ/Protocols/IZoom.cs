﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    /// <summary>
    /// Provides zoom adjustment functionality
    /// </summary>
    public interface IZoom
    {
        /// <summary>
        /// Can set zoom level directly.
        /// </summary>
        bool CanDirectZoom { get; }

        /// <summary>
        /// Can set zoom level indirectly (incremental).
        /// </summary>
        bool CanRelativeZoom { get; }

        /// <summary>
        /// Gets or sets current zoom level of the Camera.
        /// </summary>
        double Current { get; set; }

        /// <summary>
        /// Maximum possible zoom level.
        /// </summary>
        double Max { get; }

        /// <summary>
        /// Minimum possible zoom level.
        /// </summary>
        double Min { get; }

        void ZoomIn(bool stop);
        void ZoomOut(bool stop);
    }
}
