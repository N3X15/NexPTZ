﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    /// <summary>
    /// Interface controlling pan and tilt.
    /// </summary>
    public interface IMove
    {

        /// <summary>
        /// Can move to specific coordinates.
        /// </summary>
        bool CanAbsoluteMove { get; }

        /// <summary>
        /// Can move to offset coordinates.
        /// </summary>
        bool CanRelativeMove { get; }

        /// <summary>
        /// Can move diagonally.
        /// </summary>
        bool CanDiagonalMove { get; }

        /// <summary>
        /// Affects pan speed and whatnot.
        /// </summary>
        int SpeedMin { get; }
        int SpeedMax { get; }
    }
}
