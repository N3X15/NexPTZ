﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.Protocols
{
    public abstract class Protocol
    {
        [NonSerialized]
        public Camera Camera;

        public IMove Move { get; set; }
        public IZoom Zoom { get; set; }
        public IFocus Focus { get; set; }
        public IPresets Presets { get; set; }

        /// <summary>
        /// Stuff to do to connect to the camera. (Authorization, handshakes, etc.)
        /// </summary>
        public virtual void Connect() { }

        /// <summary>
        /// Stuff to do after disconnecting from the camera.
        /// </summary>
        public virtual void Disconnect() {}

        public virtual void LoadPresets() {}

        /// <summary>
        /// All the magic happens here.
        /// </summary>
        /// <param name="cmd">PTZ command to send.</param>
        /// <param name="stop">Start (false) or stop (true) the command.</param>
        /// <param name="args">Any additional information to send with the command.</param>
        public abstract void SendCommand(PTZCommand cmd, bool stop, params object[] args);

        public void MoveTo(PTZCommand cmd, bool stop)
        {
            SendCommand(cmd, stop);
        }

        public virtual void MoveToPosition(bool stop, Point2D position)
        {
            SendCommand(PTZCommand.ABSMOVE, stop, position);
        }
    }
}
