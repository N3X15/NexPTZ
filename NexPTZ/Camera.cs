﻿using NexPTZ.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ
{
    public class Camera
    {
        public string Address { get; set; }
        public int Port { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public Protocol Protocol { get; set; }

        /// <summary>
        /// Flip camera movement horizontally
        /// </summary>
        public bool FlipX { get; set; }

        /// <summary>
        /// Flip camera movement vertically
        /// </summary>
        public bool FlipY { get; set; }

        /// <summary>
        /// Rotate camera movement controls (radians)
        /// </summary>
        public double Rotate { get; set; }

        public Camera() { }
    }
}
