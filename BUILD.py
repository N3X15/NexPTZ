import os
from buildtools import os_utils, log
from buildtools.buildsystem import MSBuild
from buildtools.config import YAMLConfig
from buildtools.maestro import SingleBuildTarget, BuildMaestro

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
FRONTEND_DIR = os.path.join(SCRIPT_DIR, 'NexPTZ.WPF')

INKSCAPE = os_utils.which('inkscape') or 'C:\\Program Files\\Inkscape\\inkscape.exe'

ALL_SIZES = (64, 48, 32, 16)

log.enableANSIColors()

class SVG2PNGBuildTarget(SingleBuildTarget):
    BT_TYPE = 'SVG2PNG'

    def __init__(self, target, inputfile, height, width, dependencies=[]):
        self.height = height
        self.width = width
        super(SVG2PNGBuildTarget, self).__init__(target, files=[inputfile], dependencies=dependencies)

    def build(self):
        if self.checkMTimes(self.files, [self.target]):
            with log.info('<cyan>INKSCAPE</cyan>\t%s', os.path.relpath(self.target, SCRIPT_DIR)):
                os_utils.ensureDirExists(os.path.dirname(self.target))
                os_utils.cmd([INKSCAPE, '-z', '-e', self.target, '-h', str(self.height), '-w', str(self.width), self.files[0]], critical=True, echo=False, show_output=True)


class ICOBuildTarget(SingleBuildTarget):
    BT_TYPE = 'ICO'

    def __init__(self, target, inputfiles, dependencies=[]):
        super(ICOBuildTarget, self).__init__(target, files=inputfiles, dependencies=dependencies)

    def build(self):
        if self.checkMTimes(self.files, [self.target]):
            with log.info('<cyan>ICO</cyan>\t%s', os.path.relpath(self.target, SCRIPT_DIR)):
                os_utils.ensureDirExists(os.path.dirname(self.target))
                os_utils.cmd([os_utils.which('convert')] + [os.path.relpath(x, SCRIPT_DIR) for x in self.files] + [os.path.relpath(self.target, SCRIPT_DIR)], critical=True, echo=False, show_output=True)


maestro = BuildMaestro()
# App icon
appicons = []
for size in ALL_SIZES:
    bt = SVG2PNGBuildTarget(target=os.path.join(FRONTEND_DIR, 'tmp', 'icon-{}.png'.format(size)), inputfile=os.path.join(FRONTEND_DIR, 'svg', 'icon.svg'), height=size, width=size)
    maestro.add(bt)
    appicons += [bt.target]
appicon = ICOBuildTarget(target=os.path.join(FRONTEND_DIR, 'icons', 'NexPTZ.ico'), inputfiles=appicons)
maestro.add(appicon)
# inkscape -z -e icons/icon-16.png -w 16 -h 16 svg/icon.svg
# inkscape -z -e icons/icon-32.png -w 32 -h 32 svg/icon.svg
# inkscape -z -e icons/icon-64.png -w 64 -h 64 svg/icon.svg
# convert icons/icon-16.png icons/icon-32.png icons/icon-64.png icons/NexPTZ.ico
# pause
maestro.add(SVG2PNGBuildTarget(os.path.join(FRONTEND_DIR,'icons','ptz-knob.png'), os.path.join(FRONTEND_DIR,'svg','ptz-knob.svg'), 200, 200))
maestro.run()
config = YAMLConfig('config.yml',{'paths':{'visual-studio':'C:\\Program Files\\Microsoft Visual Studio 12.0'}})
os_utils.ensureDirExists('.tmp')
os_utils.getVSVars(config.get('paths.visual-studio'), 'x86_amd64', os.path.join('.tmp', 'getvsvars.bat'))
#os_utils.cmd(['devenv', 'NexPTZ.sln', '/rebuild', 'Release'], echo=True, show_output=True, critical=True)

os_utils.safe_rmtree(os.path.join(FRONTEND_DIR,'bin','Release'))
msb = MSBuild()
msb.configuration="Release"
msb.platform="Any CPU"
msb.solution = "NexPTZ.sln"
msb.run()
