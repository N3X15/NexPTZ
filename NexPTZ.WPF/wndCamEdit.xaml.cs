﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NexPTZ.WPF
{
    /// <summary>
    /// Interaction logic for wndCamEdit.xaml
    /// </summary>
    public partial class wndCamEdit : Window
    {
        public wndCamEdit()
        {
            InitializeComponent();
        }

        private void PopulateProtocolList()
        {
            cmbProtocol.Items.Clear();
            foreach (var protoName in Protocols.ProtocolRegistry.AllProtocols.Keys)
            {
                cmbProtocol.Items.Add(protoName);
            }
        }

        private void PopulateCameraList()
        {
            lstCameras.Items.Clear();
            foreach (PTZConfig.CameraConfig cam in PTZConfig.Config.Cameras)
            {
                lstCameras.Items.Add(cam.Name);
            }
        }

        private void lstCameras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateControls();
            UpdateDisabled();
        }

        private void UpdateControls()
        {
            PTZConfig.CameraConfig cfg = (lstCameras.SelectedIndex > -1) ? PTZConfig.Config.Cameras[lstCameras.SelectedIndex] : null;
            txtAddress.Text = cfg != null ? cfg.Address : "";
            txtName.Text = cfg != null ? cfg.Name : "";
            txtPassword.Password = cfg != null ? cfg.Password : "";
            txtUsername.Text = cfg != null ? cfg.Username : "";
            cmbProtocol.SelectedItem = cfg != null ? cfg.ProtocolID : "";
            iudPort.Value = cfg != null ? cfg.Port : 80;
            iudRotate.Value = cfg != null ? (int)Math.Round(cfg.Rotation*90d) : 0;
            chkFlipX.IsChecked = cfg != null ? cfg.FlipX : false;
            chkFlipY.IsChecked = cfg != null ? cfg.FlipY : false;
        }

        private void UpdateDisabled()
        {
            bool selected = lstCameras.SelectedIndex > -1;
            txtAddress.IsEnabled = selected;
            txtName.IsEnabled = selected;
            txtPassword.IsEnabled = selected;
            txtUsername.IsEnabled = selected;
            iudPort.IsEnabled = selected;
            iudRotate.IsEnabled = selected;
            cmdDeleteCamera.IsEnabled = selected;
            chkFlipX.IsEnabled = selected;
            chkFlipY.IsEnabled = selected;
        }

        private void cmdAddCamera_Click(object sender, RoutedEventArgs e)
        {
            CreateCamera();
        }
        public void CreateCamera()
        {
            var cam = new PTZConfig.CameraConfig();
            PTZConfig.Config.Cameras.Add(cam);
            PopulateCameraList();
            lstCameras.SelectedIndex = lstCameras.Items.Count - 1;
            UpdateControls();
            UpdateDisabled();
            Validate();
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validate();
        }

        private void Validate()
        {
            cmdSave.IsEnabled = lstCameras.IsEnabled = doValidate();
        }

        private bool doValidate()
        {
            if (lstCameras.SelectedIndex == -1)
                return true;
            if (string.IsNullOrEmpty(txtName.Text))
                return false;
            if (string.IsNullOrEmpty(txtAddress.Text))
                return false;
            if (!iudPort.Value.HasValue || iudPort.Value <= 0)
                return false;
            if (cmbProtocol.SelectedIndex == -1)
                return false;
            return true;
        }

        private void txtAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validate();
        }

        private void iudPort_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Validate();
        }

        private void cmbProtocol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }

        private void txtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validate();
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Validate();
        }

        private void cmdSave_Click(object sender, RoutedEventArgs e)
        {
            if (doValidate())
            {
                PTZConfig.CameraConfig cfg = PTZConfig.Config.Cameras[lstCameras.SelectedIndex];
                cfg.Address = txtAddress.Text;
                cfg.Name = txtName.Text;
                cfg.Password = txtPassword.Password;
                cfg.Username = txtUsername.Text;
                cfg.ProtocolID = (string)cmbProtocol.SelectedItem;
                cfg.Port = iudPort.Value.Value;
                cfg.FlipX = chkFlipX.IsChecked.GetValueOrDefault(false);
                cfg.FlipY = chkFlipY.IsChecked.GetValueOrDefault(false);
                cfg.Rotation = iudRotate.Value.GetValueOrDefault(0)/90;
                PopulateCameraList();
                PTZConfig.Save();
                UpdateDisabled();
                cmdSave.IsEnabled = lstCameras.IsEnabled = true;
            }
        }

        private void Window_Initialized(object sender, EventArgs e)
        {

            PopulateCameraList();
            PopulateProtocolList();
            lstCameras.SelectedIndex = -1;
            UpdateControls();
            UpdateDisabled();
        }
    }
}
