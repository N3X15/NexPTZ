﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPTZ.WPF
{
    public class Utils
    {

        public static Point2D ConvertPoint(System.Windows.Point p)
        {
            Point2D P = new Point2D();
            P.X = p.X;
            P.Y = p.Y;
            return P;
        }
    }
}
