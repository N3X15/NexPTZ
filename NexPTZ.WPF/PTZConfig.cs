﻿using Nett;
using NexPTZ.Protocols;
using System;
using System.Collections.Generic;
using System.IO;

namespace NexPTZ.WPF
{
    public class PTZConfig
    {
        public static GlobalConfig Config;

        [Serializable]
        public class CameraConfig
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public int Port { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }

            public string ProtocolID { get; set; }

            public bool FlipX { get; set; }
            public bool FlipY { get; set; }
            public double Rotation { get; set; }

            public Camera AsCamera()
            {
                Camera cam = new Camera();
                if (!string.IsNullOrEmpty(this.ProtocolID))
                {
                    cam.Protocol = ProtocolRegistry.GetProtocol(this.ProtocolID);
                    cam.Protocol.Camera = cam;
                }
                cam.Address = Address;
                cam.Password = Password;
                cam.Port = Port;
                cam.Username = Username;
                cam.FlipX = FlipX;
                cam.FlipY = FlipY;
                cam.Rotate = Rotation;

                return cam;
            }
        }

        [Serializable]
        public class GlobalConfig
        {
            public List<CameraConfig> Cameras { get; set; }
        }

        public static void Load()
        {
            Config = new GlobalConfig();
            Config.Cameras = new List<CameraConfig>();
            ProtocolRegistry.Preload();
            if (File.Exists("config.toml"))
                Config = Toml.ReadFile<GlobalConfig>("config.toml");
        }

        public static void Save()
        {
            Toml.WriteFile<GlobalConfig>(Config, "config.toml");
        }

        public static Camera GetCamera(string name = "")
        {
            CameraConfig camcfg = null;
            if (name == "")
            {
                foreach (CameraConfig _camcfg in Config.Cameras)
                {
                    camcfg = _camcfg;
                    break;
                }
            }
            else
            {
                foreach (CameraConfig _camcfg in Config.Cameras)
                {
                    if (_camcfg.Name == name)
                    {
                        camcfg = _camcfg;
                        break;
                    }
                }
            }
            if (camcfg != null)
            {
                return camcfg.AsCamera();
            }
            else
            {
                return null;
            }
        }
    }
}