﻿using NexPTZ;
using NexPTZ.Protocols;
using Parago.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NexPTZ.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const double ARC = Math.PI / 8;
        private Camera Camera;
        private PTZCommand LastCommand = PTZCommand.RIGHT;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PopulateCameras()
        {
            cmbActiveCamera.Items.Clear();
            foreach (PTZConfig.CameraConfig cam in PTZConfig.Config.Cameras)
            {
                cmbActiveCamera.Items.Add(cam.Name);
            }
        }

        private void UpdateControls()
        {
            imgKnob.IsEnabled = Camera != null;
            cmdAddPreset.IsEnabled = Camera?.Protocol?.Presets?.CanGotoPreset == true;
            sldSpeed.IsEnabled = Camera != null;

            cmdZoomIn.IsEnabled = cmdZoomOut.IsEnabled = Camera?.Protocol?.Zoom?.CanRelativeZoom == true;
            dudZoom.IsEnabled = Camera?.Protocol?.Zoom?.CanDirectZoom == true;
            if(Camera?.Protocol?.Zoom!=null)
            {
                if(Camera?.Protocol?.Zoom?.CanDirectZoom==true)
                {
                    dudZoom.Value = Camera.Protocol.Zoom.Current;
                }
            }

            if (Camera?.Protocol?.Move != null)
            {
                sldSpeed.Maximum = Camera.Protocol.Move.SpeedMax;
                sldSpeed.Minimum = Camera.Protocol.Move.SpeedMin;
            }
            lstPresets.IsEnabled = Camera?.Protocol?.Presets != null;
            cmdAddPreset.IsEnabled = Camera?.Protocol?.Presets != null;

            PopulatePresets();
        }

        private void PopulatePresets()
        {
            lstPresets.Items.Clear();
            if (Camera?.Protocol?.Presets != null)
            {
                lstPresets.IsEnabled = true;
                cmdAddPreset.IsEnabled = true;
                if (Camera?.Protocol != null)
                {
                    foreach (var presetName in Camera.Protocol.Presets.AllPresets)
                    {
                        lstPresets.Items.Add(presetName);
                    }
                }
            }
        }

        private void DisplayCamEdit()
        {
            wndCamEdit wce = new wndCamEdit();
            wce.ShowDialog();
        }

        private void imgKnob_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point2D positionOffset = Utils.ConvertPoint(e.GetPosition(imgKnob));
            positionOffset.X = (positionOffset.X - (imgKnob.ActualWidth / 2)) / imgKnob.ActualWidth;
            positionOffset.Y = (positionOffset.Y - (imgKnob.ActualHeight / 2)) / imgKnob.ActualHeight;
            double angle = Math.Atan2(positionOffset.X, positionOffset.Y);
            if (Camera.Rotate != 0d) // rotate90
            {
                /*angle -= (Math.PI / 2);*/
                angle -= Camera.Rotate * (Math.PI / 2);
                if (angle < -Math.PI)
                {
                    angle += (2 * Math.PI);
                }
            }
            if (Camera.FlipX) // flip X
            {

                if (angle <= 0)
                    angle = -Math.PI - angle;
                else
                    angle = Math.PI - angle;
            }
            if (Camera.FlipY)
            {
                angle = angle * -1;
            }

            if (Camera?.Protocol?.Move!=null)
            {
                PTZCommand? cmd = null;
                if (Camera?.Protocol?.Move?.CanRelativeMove == true)
                {
                    SendCommand(PTZCommand.RELMOVE, false, positionOffset, 0);
                    return;
                }
                else
                {
                    // This part stolen from iSpy and modified gently.
                    // I don't think they'd care, but credit given where credit due.
                    if (angle < ARC && angle > -ARC)
                    {
                        cmd = PTZCommand.LEFT;
                    }
                    if (angle >= ARC && angle < 3 * ARC)
                    {
                        cmd = PTZCommand.UPLEFT;
                    }
                    if (angle >= 3 * ARC && angle < 5 * ARC)
                    {
                        cmd = PTZCommand.UP;
                    }
                    if (angle >= 5 * ARC && angle < 7 * ARC)
                    {
                        cmd = PTZCommand.UPRIGHT;
                    }
                    if (angle >= 7 * ARC || angle < -7 * ARC)
                    {
                        cmd = PTZCommand.RIGHT;
                    }
                    if (angle <= -5 * ARC && angle > -7 * ARC)
                    {
                        cmd = PTZCommand.DOWNRIGHT;
                    }
                    if (angle <= -3 * ARC && angle > -5 * ARC)
                    {
                        cmd = PTZCommand.DOWN;
                    }
                    if (angle <= -ARC && angle > -3 * ARC)
                    {
                        cmd = PTZCommand.DOWNLEFT;
                    }
                }
                if (cmd.HasValue)
                    SendCommand(cmd.Value, false, (int)sldSpeed.Value);
            }
        }

        private void SendCommand(PTZCommand cmd, bool stop, params object[] args)
        {
            Camera.Protocol.SendCommand(cmd, stop, args);
            if (!stop)
                LastCommand = cmd;
        }

        private void imgKnob_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Camera?.Protocol?.Move?.CanRelativeMove==true)
            {
                SendCommand(PTZCommand.RELMOVE, true, new Point2D() { X = 0, Y = 0 }, (int)sldSpeed.Value);
            }
            else
            {
                SendCommand(LastCommand, true, (int)sldSpeed.Value);
            }
        }

        private void cmdZoomIn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Camera.Protocol.Zoom.ZoomIn(false);
        }

        private void cmdZoomIn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Camera.Protocol.Zoom.ZoomIn(true);
            dudZoom.Value = Camera.Protocol.Zoom.Current;
        }

        private void cmdZoomOut_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Camera.Protocol.Zoom.ZoomOut(false);
        }

        private void cmdZoomOut_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Camera.Protocol.Zoom.ZoomOut(true);
            dudZoom.Value = Camera.Protocol.Zoom.Current;
        }

        private void cmdAddPreset_Click(object sender, RoutedEventArgs e)
        {
            Camera.Protocol.Presets.AddPreset("");
            PopulatePresets();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            try
            {
                PTZConfig.Load();
                if (PTZConfig.Config.Cameras.Count > 0)
                {
                    PopulateCameras();
                    Camera = PTZConfig.Config.Cameras[0].AsCamera();
                    cmbActiveCamera.SelectedItem = PTZConfig.Config.Cameras[0].Name;
                    ConnectToCamera();
                }
                else
                    DisplayCamEdit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
                Console.WriteLine(ex);
            }
        }

        private void ConnectToCamera()
        {
            ProgressDialog.Execute(null, "Connecting to camera...", () =>
            {
                if (Camera != null && Camera.Protocol != null)
                {
                    ProgressDialog.Current.Report("Connecting to camera...");
                    Camera.Protocol.Connect();
                    ProgressDialog.Current.Report("Loading presets...");
                    Camera.Protocol.LoadPresets();
                }
                else
                {
                    if (Camera != null && Camera.Protocol == null)
                        MessageBox.Show("Failed to load protocol for this camera. Please check its settings.", "Protocol Failure");
                }
            }, new ProgressDialogSettings(false, false, true));

            PopulatePresets();
            UpdateControls();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            DisplayCamEdit();
            PopulateCameras();
        }

        private void cmbActiveCamera_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Camera = PTZConfig.GetCamera((string)cmbActiveCamera.SelectedItem);
            ConnectToCamera();
        }

        private void mnuPresetGoto_Click(object sender, RoutedEventArgs e)
        {
            if (Camera?.Protocol?.Presets != null)
                Camera.Protocol.Presets.GoToPreset((string)lstPresets.SelectedItem);
        }

        private void mnuPresetSet_Click(object sender, RoutedEventArgs e)
        {
            if (Camera?.Protocol?.Presets != null)
                Camera.Protocol.Presets.SetPreset((string)lstPresets.SelectedItem);
            PopulatePresets();
        }

        private void mnuPresetClear_Click(object sender, RoutedEventArgs e)
        {
            if (Camera?.Protocol?.Presets != null)
                Camera.Protocol.Presets.RemovePreset((string)lstPresets.SelectedItem);
            PopulatePresets();
        }

        private void lstPresets_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Camera?.Protocol?.Presets != null)
                Camera.Protocol.Presets.GoToPreset((string)lstPresets.SelectedItem);
        }

        private void mnuTopMost_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Topmost = {0}", Topmost = mnuTopMost.IsChecked);
        }

        private void dudZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (Camera?.Protocol?.Zoom?.CanDirectZoom == true)
                Camera.Protocol.Zoom.Current = dudZoom.Value.Value;
        }
    }
}
